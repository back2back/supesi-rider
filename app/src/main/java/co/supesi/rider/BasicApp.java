package co.supesi.rider;

import android.app.Application;

import com.facebook.stetho.Stetho;

public class BasicApp extends Application {
    private static BasicApp mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

        if(BuildConfig.DEBUG){
            Stetho.initialize(Stetho.newInitializerBuilder(this)
                    .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                    .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                    .build());
        }
    }

    public static BasicApp getContext(){return mContext;}
}
