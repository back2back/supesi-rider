package co.supesi.rider;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Locations {
    @PrimaryKey(autoGenerate = true)

    private int id;
    private double lat;
    private double lng;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
