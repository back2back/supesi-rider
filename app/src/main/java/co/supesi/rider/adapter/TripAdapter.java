package co.supesi.rider.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.supesi.rider.MainActivity;
import co.supesi.rider.R;
import co.supesi.rider.model.ApiTripResponse;
import co.supesi.rider.model.OrderEntry;
import co.supesi.rider.model.Store;
import co.supesi.rider.model.Trip;
import co.supesi.rider.model.User;
import co.supesi.rider.network.NetworkData;
import co.supesi.rider.network.RetrofitClientInstance;
import co.supesi.rider.utility.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TripAdapter extends RecyclerView.Adapter<TripAdapter.TripViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<Trip> tripArrayList;
    SharedPreferences sharedPref;
    String token;
    private static final String TAG = "TripAdapter";

    public TripAdapter(Context context, ArrayList<Trip> tripArrayList){
        layoutInflater = LayoutInflater.from(context);
        this.tripArrayList = tripArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public TripViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.trip_card, parent, false);
        sharedPref = context.getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);
        token = sharedPref.getString("jwt",null);
        return new TripViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TripViewHolder holder, int position) {
        Trip trip = tripArrayList.get(position);
        Log.e(TAG, "nice trip" + trip.getNiceAmount());
        String startLat = trip.getStartLat();
        String startLng = trip.getStartLng();
        String endLat = trip.getEndLat();
        String endLng = trip.getEndLng();

        String startPoint = "geo:"+startLat+","+startLng+"?q="+startLat+","+startLng;
        String endPoint = "geo:"+endLat+","+endLng+"?q="+endLat+","+endLng;
        int tripAmount = trip.getNiceAmount();
        holder.tripAmount.setText(String.valueOf(tripAmount));
        holder.shopMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri gmmIntentUri = Uri.parse(startPoint);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                context.startActivity(mapIntent);
            }
        });

        holder.customerMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse(endPoint);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                context.startActivity(mapIntent);
            }
        });



        OrderEntry orderEntry = trip.getOrderEntry();
        int goodsAmount = orderEntry.getNiceAmount();
        User user = orderEntry.getCustomer();
        holder.callCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData((Uri.parse(user.getPhone())));
                context.startActivity(intent);
            }
        });
        Log.e(TAG, user.getEmail());
        holder.customerName.setText(user.getfName());
        Store store = orderEntry.getStore();
        holder.shopName.setText(store.getName());
        User storeKeeper = store.getStoreUser();

        holder.callShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(storeKeeper.getPhone()));
                context.startActivity(intent);
            }
        });
        holder.endTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //launch dialog fragment
                alertEndTrip(trip);
                //endTrip(trip.getId());
            }
        });
        holder.totalAmount.setText((String.valueOf(tripAmount+ goodsAmount)));

    }
    private void alertEndTrip(Trip trip) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Full amount paid? End trip?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Log.e(TAG, "okay");
                        endTrip(trip.getId());
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.e(TAG, "cancel");
                    }
                })
        ;
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void endTrip(int id) {
        Trip tripFinish = new Trip();
        tripFinish.setStatus(1);
        NetworkData networkData = RetrofitClientInstance.getRetrofitInstance().create(NetworkData.class);
        Call<ApiTripResponse> tripCall = networkData.endTrip(token, id, tripFinish);
        tripCall.enqueue(new Callback<ApiTripResponse>() {
            @Override
            public void onResponse(Call<ApiTripResponse> call, Response<ApiTripResponse> response) {
                if (response.body() != null){
                    Log.e(TAG, "successfully completed");
                    parseResponse(response.body());

                }

            }

            @Override
            public void onFailure(Call<ApiTripResponse> call, Throwable t) {
                Log.e(TAG, "cannot  completed");
            }
        });
    }

    private void parseResponse(ApiTripResponse body) {
        String response = body.getMessage();
        if (Boolean.parseBoolean(response)){
            //launch an activity with
            tripArrayList.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return tripArrayList.size();
    }

    class TripViewHolder extends RecyclerView.ViewHolder{

        //text views
        @BindView(R.id.tv_trip_amount)
        TextView tripAmount;
        @BindView(R.id.tv_shop_name)
        TextView shopName;
        @BindView(R.id.tv_customer_name)
        TextView customerName;
        @BindView(R.id.tv_total_amount)
        TextView totalAmount;
        //buttons
        @BindView(R.id.btn_map_customer)
        Button customerMap;
        @BindView(R.id.btn_map_shop)
        Button shopMap;
        @BindView(R.id.btn_call_customer)
        Button callCustomer;
        @BindView(R.id.btn_call_shop)
        Button callShop;
        @BindView(R.id.btn_end_trip)
        Button endTrip;
        public TripViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
