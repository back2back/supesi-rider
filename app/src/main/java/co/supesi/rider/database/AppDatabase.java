package co.supesi.rider.database;

import android.content.Context;
import android.location.Location;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import co.supesi.rider.Locations;
import co.supesi.rider.model.User;

@Database(entities = { Locations.class, User.class}, version = 8)
public abstract class AppDatabase extends RoomDatabase {

    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    private static AppDatabase INSTANCE;

    public abstract LocationDao locationDao();
    public abstract UserDao userDao();

    public static AppDatabase getAppDatabase(Context context){
        if (INSTANCE == null){
            synchronized (AppDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),AppDatabase.class, "supesi_rider")
                            .addCallback(mCallback)
                            .fallbackToDestructiveMigration()
                            .allowMainThreadQueries()
                            .build();
                }
            }

        }
        return INSTANCE;
    }

    //for demo, every time we open the connection delete what was there previously
    private static RoomDatabase.Callback mCallback = new RoomDatabase.Callback(){
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            // TODO: 6/1/20 remove this later
            databaseWriteExecutor.execute(()->{
                LocationDao locationDao = INSTANCE.locationDao();
                locationDao.deleteAll();

            });
        }
    };
}
