package co.supesi.rider.database;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import co.supesi.rider.Locations;
import co.supesi.rider.model.User;

public class DataRepository {

    //location
    LocationDao locationDao;
    LiveData<List<Locations>> locations;

    //user
    UserDao userDao;
    MutableLiveData<User> user;

    AppDatabase db;

    public DataRepository(Application application) {
        db = AppDatabase.getAppDatabase(application);

        locationDao = db.locationDao();
        userDao = db.userDao();
    }

    //location
    public void insertLocation(Locations locations){
        AppDatabase.databaseWriteExecutor.execute(()-> {
            locationDao.insert(locations);
        });
    }
    public void deleteAllLocations(){
        AppDatabase.databaseWriteExecutor.execute(()-> {
            locationDao.deleteAll();
        });
    }

    public LiveData<List<Locations>> getLocations(){
        return locations;
    }

    //user
    public void insertUser(User user){
        AppDatabase.databaseWriteExecutor.execute(()->{
            userDao.insert(user);
        });
    }


    public MutableLiveData<User> getUser(){
        if (user == null){
            user = new MutableLiveData<>();
        }
        return  user;
    }
    public User getSingleUser(){
        return userDao.getSingleUser();
    }

}
