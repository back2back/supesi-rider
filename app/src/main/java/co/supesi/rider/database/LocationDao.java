package co.supesi.rider.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import co.supesi.rider.Locations;

@Dao
public interface LocationDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Locations...locations);

    @Query("DELETE from locations")
    void deleteAll();
}
