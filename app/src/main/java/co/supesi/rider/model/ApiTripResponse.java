package co.supesi.rider.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ApiTripResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("trips")
    ArrayList<Trip> trips;
    @SerializedName("trip")
    Trip singleTrip;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Trip> getTrips() {
        return trips;
    }

    public void setTrips(ArrayList<Trip> trips) {
        this.trips = trips;
    }

    public Trip getSingleTrip() {
        return singleTrip;
    }

    public void setSingleTrip(Trip singleTrip) {
        this.singleTrip = singleTrip;
    }
}
