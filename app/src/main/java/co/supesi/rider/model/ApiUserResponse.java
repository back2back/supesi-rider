package co.supesi.rider.model;

import com.google.gson.annotations.SerializedName;

public class ApiUserResponse {
    @SerializedName("jwt")
    private String jwt;
    @SerializedName("user")
    private User singleUser;
    @SerializedName("message")
    private String message;

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public User getSingleUser() {
        return singleUser;
    }

    public void setSingleUser(User singleUser) {
        this.singleUser = singleUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
