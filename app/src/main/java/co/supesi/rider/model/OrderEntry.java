package co.supesi.rider.model;

import com.google.gson.annotations.SerializedName;

public class OrderEntry {
    @SerializedName("id")
    private int id;
    @SerializedName("user")
    private User customer;
    @SerializedName("store")
    private Store store;
    @SerializedName("nice_amount")
    private int niceAmount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public int getNiceAmount() {
        return niceAmount;
    }

    public void setNiceAmount(int niceAmount) {
        this.niceAmount = niceAmount;
    }
}
