package co.supesi.rider.model;

import com.google.gson.annotations.SerializedName;

public class Store {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("user")
    private User storeUser;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getStoreUser() {
        return storeUser;
    }

    public void setStoreUser(User storeUser) {
        this.storeUser = storeUser;
    }
}
