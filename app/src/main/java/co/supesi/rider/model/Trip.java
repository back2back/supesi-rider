package co.supesi.rider.model;

import com.google.gson.annotations.SerializedName;

public class Trip {
    @SerializedName("id")
    private int id;
    @SerializedName("status")
    private int status;
    @SerializedName("start_lat")
    private String startLat;
    @SerializedName("start_lng")
    private String startLng;
    @SerializedName("end_lat")
    private String endLat;
    @SerializedName("end_lng")
    private String endLng;
    @SerializedName("nice_amount")
    private int niceAmount;
    @SerializedName("order_entry")
    private OrderEntry orderEntry;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStartLat() {
        return startLat;
    }

    public void setStartLat(String startLat) {
        this.startLat = startLat;
    }

    public String getStartLng() {
        return startLng;
    }

    public void setStartLng(String startLng) {
        this.startLng = startLng;
    }

    public String getEndLat() {
        return endLat;
    }

    public void setEndLat(String endLat) {
        this.endLat = endLat;
    }

    public String getEndLng() {
        return endLng;
    }

    public void setEndLng(String endLng) {
        this.endLng = endLng;
    }

    public int getNiceAmount() {
        return niceAmount;
    }

    public void setNiceAmount(int niceAmount) {
        this.niceAmount = niceAmount;
    }

    public OrderEntry getOrderEntry() {
        return orderEntry;
    }

    public void setOrderEntry(OrderEntry orderEntry) {
        this.orderEntry = orderEntry;
    }
}
