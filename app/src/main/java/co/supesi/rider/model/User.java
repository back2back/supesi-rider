package co.supesi.rider.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity
public class User {

    @SerializedName("id")
    @PrimaryKey
    @NonNull
    private int id;
    @SerializedName("email")
    private String email;

    @Nullable
    @Ignore
    private String password;

    @SerializedName("f_name")
    @Ignore
    private String fName;
    @SerializedName("l_name")
    @Ignore
    private String lName;
    @SerializedName("phone")
    private String phone;

    @Ignore
    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User(String email, String password, int id){
        this.email = email;
        this.password = password;
        this.id = id;
    }

    public User(int id){
         this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Nullable
    public String getPassword() {
        return password;
    }

    public void setPassword(@Nullable String password) {
        this.password = password;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getPhone() {
        return "tel:"+phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
