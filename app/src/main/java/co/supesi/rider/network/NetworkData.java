package co.supesi.rider.network;

import co.supesi.rider.model.ApiTripResponse;
import co.supesi.rider.model.ApiUserResponse;
import co.supesi.rider.model.Trip;
import co.supesi.rider.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface NetworkData {

    @POST("users/login.json")
    Call<ApiUserResponse> login(@Body User user);
    @GET("trips.json")
    Call<ApiTripResponse> myTrip(@Header("Authorization") String jwt);
    @PUT("trips/{id}.json")
    Call<ApiTripResponse> endTrip(@Header("Authorization") String jwt ,@Path("id") int id, @Body Trip trip);
}
