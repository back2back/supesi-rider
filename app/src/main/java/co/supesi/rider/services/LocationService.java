package co.supesi.rider.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Looper;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.lifecycle.LifecycleService;
import androidx.lifecycle.Observer;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;

import co.supesi.rider.BasicApp;
import co.supesi.rider.Locations;
import co.supesi.rider.MainActivity;
import co.supesi.rider.R;
import co.supesi.rider.database.DataRepository;
import co.supesi.rider.model.User;
import co.supesi.rider.model.UserLocation;
import co.supesi.rider.ui.home.LoginViewModel;

public class LocationService extends LifecycleService {

    String CHANNEL_ID = "MY_CHANNEL";
    String CHANNEL_NAME = "Amazing Channel";
    private static final String TAG = "LocationService";
    private static final String LOCK_TAG = "Supesi::LocationService";

    private FusedLocationProviderClient mFusedLocationClient;
    private final static long UPDATE_INTERVAL = 4 * 1000;  /* 4 secs */
    private final static long FASTEST_INTERVAL = 2000; /* 2 sec */

    MediaPlayer mediaPlayer;
    LocationCallback locationCallback;

    DataRepository dataRepository;
    Observer<User> userObserver;
    User out;
    LoginViewModel loginViewModel;
    PowerManager powerManager;
    PowerManager.WakeLock wakeLock;

    @Override
    public void onCreate() {
        super.onCreate();
        userObserver = new Observer<User>() {
            @Override
            public void onChanged(User user) {
                out = user;
                Log.e(TAG, "user " + out.toString());
            }
        };
        loginViewModel = new LoginViewModel(BasicApp.getContext());

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O){
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT);
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);


            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .setContentTitle("Supesi")
                    .setContentIntent(pendingIntent)
                    .setContentText("You are online").build();

            startForeground(1, notification);
        }


    }

    private void doCool() {
        mediaPlayer = MediaPlayer.create(this, Settings.System.DEFAULT_ALARM_ALERT_URI);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
    }


    private void getLocation() {

        // ---------------------------------- LocationRequest ------------------------------------
        // Create the location request to start receiving updates
        LocationRequest mLocationRequestHighAccuracy = new LocationRequest();
        mLocationRequestHighAccuracy.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequestHighAccuracy.setInterval(UPDATE_INTERVAL);
        mLocationRequestHighAccuracy.setFastestInterval(FASTEST_INTERVAL);


        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "getLocation: stopping the location service.");
            stopSelf();
            return;
        }
        Log.d(TAG, "getLocation: getting location information.");
        locationCallback =new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {

                Log.e(TAG, "onLocationResult: got location result.");

                Location location = locationResult.getLastLocation();

                if (location != null) {

                    saveUserLocationToLocal(location);
                    GeoPoint geoPoint = new GeoPoint(location.getLatitude(), location.getLongitude());
                    dataRepository = new DataRepository(BasicApp.getContext());


                    UserLocation userLocation = new UserLocation(geoPoint, dataRepository.getSingleUser());
                    saveUserLocationOnline(userLocation);




                }
            }
        };
        mFusedLocationClient.requestLocationUpdates(mLocationRequestHighAccuracy,
                locationCallback,
                Looper.myLooper()); // Looper.myLooper tells this to repeat forever until thread is destroyed
    }


    private void saveUserLocationOnline(UserLocation userLocation) {

        try {
            DocumentReference locationRef = FirebaseFirestore.getInstance()
                    .collection(getString(R.string.location_ref))
            // TODO: 6/1/20 simulate shit for now replace with actual data
                    .document(String.valueOf(userLocation.getUser().getId()));

            locationRef.set(userLocation).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()){
                        Log.e(TAG, "user insert" );
                    }
                }
            });

        }catch (NullPointerException e){
            Log.e(TAG, "Location is null, can not save online " + e.getMessage() );
            stopSelf();
        }
    }

    private void saveUserLocationToLocal(Location location){
        //save this location to our local db
        Locations manyLocations = new Locations();
        manyLocations.setLat(location.getLatitude());
        manyLocations.setLng(location.getLongitude());

        Log.e(TAG, "lat " + location.getLatitude());
        Log.e(TAG, "lng " + location.getLongitude());
        DataRepository dataRepository = new DataRepository(BasicApp.getContext());
        dataRepository.insertLocation(manyLocations);

        
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //doCool();
        super.onStartCommand(intent, flags, startId);
        getLocation();
        getWakeLock();

        Log.e(TAG, "onStartCommand called");
        return START_NOT_STICKY;
    }

    private void getWakeLock() {
        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                LOCK_TAG);
        wakeLock.acquire();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy called");
        if (mFusedLocationClient != null){
            mFusedLocationClient.removeLocationUpdates(locationCallback);
        }

        releaseWakeLock();
        stopSelf();

    }

    private void releaseWakeLock() {
        wakeLock.release();
    }


}
