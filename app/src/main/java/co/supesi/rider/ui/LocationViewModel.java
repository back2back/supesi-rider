package co.supesi.rider.ui;

import android.app.Application;
import android.app.ListActivity;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import co.supesi.rider.Locations;
import co.supesi.rider.database.DataRepository;

public class LocationViewModel extends AndroidViewModel {

    private static final String TAG = "LocationViewModel";
    private DataRepository dataRepository;

    private LiveData<List<Locations>> locations;
    public LocationViewModel(@NonNull Application application) {
        super(application);
        dataRepository = new DataRepository(application);
        locations = dataRepository.getLocations();
    }

    public LiveData<List<Locations>> getLocations(){return locations;}
    public void insertLocation(Locations locations){
        dataRepository.insertLocation(locations);
    }
}
