package co.supesi.rider.ui.home;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.rider.R;
import co.supesi.rider.adapter.TripAdapter;
import co.supesi.rider.model.ApiTripResponse;
import co.supesi.rider.model.OrderEntry;
import co.supesi.rider.model.Store;
import co.supesi.rider.model.Trip;
import co.supesi.rider.model.User;
import co.supesi.rider.network.NetworkData;
import co.supesi.rider.network.RetrofitClientInstance;
import co.supesi.rider.services.LocationService;
import co.supesi.rider.utility.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.ACTIVITY_SERVICE;

public class HomeFragment extends Fragment {

    Intent serviceIntent;
    private LoginViewModel loginViewModel;
    private Unbinder unbinder;
    private final static String TAG = "HomeFragment";
    private static final String SERVICE_NAME = "co.supesi.rider.services.LocationService";
    private String token;
    private SharedPreferences sharedPref;

    @BindView(R.id.btn_online)
    Button online;
    @BindView(R.id.rv_trips)
    RecyclerView recyclerView;
    TripAdapter tripAdapter;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        online = root.findViewById(R.id.btn_online);

        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final NavController navController = Navigation.findNavController(view);

        loginViewModel = new ViewModelProvider(requireActivity()).get(LoginViewModel.class);
        loginViewModel.getAuthState().observe(requireActivity(), new Observer<LoginViewModel.AuthenticationState>() {
            @Override
            public void onChanged(LoginViewModel.AuthenticationState authenticationState) {
                if (authenticationState == LoginViewModel.AuthenticationState.UNAUTHENTICATED) {
                    navController.navigate(R.id.nav_login);
                    Log.e(TAG, "case go away");
                }
            }
        });

        onlineOfflineButton();

        loadToken(view);
        fetchOnline();
    }

    private void fetchOnline(){
        NetworkData networkData = RetrofitClientInstance.getRetrofitInstance().create(NetworkData.class);
        Call<ApiTripResponse> listCall = networkData.myTrip(token);
        listCall.enqueue(new Callback<ApiTripResponse>() {
            @Override
            public void onResponse(Call<ApiTripResponse> call, Response<ApiTripResponse> response) {
                if (response.body() != null){
                    processResponse(response.body());
                    Log.e(TAG, "got response");
                }
            }

            @Override
            public void onFailure(Call<ApiTripResponse> call, Throwable t) {
                    Log.e(TAG, "failed to load trip");
            }
        });
    }

    private void processResponse(ApiTripResponse body) {
        int size = body.getTrips().size();
        if (size > 0){
            for (int y = 0; y <size; y++){
                Trip trip = body.getTrips().get(y);

                loadTrip(body.getTrips());
            }
        }

    }

    private void loadToken(View view) {
        sharedPref = view.getContext().getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);
        token = sharedPref.getString("jwt",null);
    }

    private void loadTrip(ArrayList<Trip> trips) {
        tripAdapter = new TripAdapter(getContext(),trips);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(tripAdapter);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        tripAdapter = null;

    }

    private boolean isLocationServiceRunning() {
        ActivityManager manager = (ActivityManager) getActivity(). getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)){
            if(SERVICE_NAME.equals(service.service.getClassName())) {
                Log.e(TAG, "isLocationServiceRunning: location service is already running.");
                online.setText("Online");
                return true;
            }
        }
        online.setText("offline");
        Log.e(TAG, "isLocationServiceRunning: location service is not running.");
        return false;
    }
    private void startLocationService(){

        if(!isLocationServiceRunning()){
            serviceIntent = new Intent(getActivity(), LocationService.class);


            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O){

                getActivity().startForegroundService(serviceIntent);
            }else{
                getActivity().startService(serviceIntent);
            }
        }
    }
    private void stopLocationService() {
        serviceIntent = new Intent(getActivity(), LocationService.class);

        getActivity().stopService(serviceIntent);
        Log.e(TAG, "stopping the service clicked");

    }

    public void onlineOfflineButton(){


        online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "oncllic");
                // Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //       .setAction("Action", null).show();
                if (!isLocationServiceRunning()){
                    //start service
                    startLocationService();

                }
                else {
                    Log.e(TAG, "attempt to stop");
                    stopLocationService();
                    //online.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_menu_send));
                }


            }
        });

    }
}
