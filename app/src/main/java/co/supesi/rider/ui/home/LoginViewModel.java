package co.supesi.rider.ui.home;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import co.supesi.rider.database.DataRepository;
import co.supesi.rider.model.ApiUserResponse;
import co.supesi.rider.model.User;
import co.supesi.rider.network.NetworkData;
import co.supesi.rider.network.RetrofitClientInstance;
import co.supesi.rider.utility.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends AndroidViewModel {

    public static final String TAG = "LoginViewModel";

    public enum AuthenticationState{
        UNAUTHENTICATED,
        AUTHENTICATED,
        INVALID_AUTHENTICATION
    }

    String jwt;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    final MutableLiveData<AuthenticationState> authState = new MutableLiveData<>();

    private DataRepository dataRepository;
    private LiveData<User> user;

    public LoginViewModel(Application application){
        super(application);
        dataRepository = new DataRepository(application);
        user = dataRepository.getUser();

        preferences = getApplication().getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);
        jwt = preferences.getString("jwt",null);


        if (jwt == null){
            authState.setValue(AuthenticationState.UNAUTHENTICATED);

        }else{
            authState.setValue(AuthenticationState.AUTHENTICATED);

        }



    }
    public LiveData<User> getUser(){
        return user;
    }
    public void authenticateOnline(String email, String password){
        isLoginValid(email,password);
    }

    public MutableLiveData<AuthenticationState> getAuthState(){
        return authState;
    }

    private void writePref(String jwt){
        preferences = getApplication().getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString("jwt", jwt);
        editor.apply();

    }

    private void isLoginValid(String email, String password){
        NetworkData networkData =  RetrofitClientInstance.getRetrofitInstance().create(NetworkData.class);
        Call<ApiUserResponse> loginCall = networkData.login(new User(email, password));
        loginCall.enqueue(new Callback<ApiUserResponse>() {
            @Override
            public void onResponse(Call<ApiUserResponse> call, Response<ApiUserResponse> response) {
                if (response.body() != null){
                    ApiUserResponse apiUserResponse = response.body();
                    boolean msg = Boolean.parseBoolean(apiUserResponse.getMessage());

                    if (msg){
                        //write to shared pref
                        writePref(apiUserResponse.getJwt());
                        LoginViewModel.this.authState.setValue(AuthenticationState.AUTHENTICATED);
                        Log.e(TAG, "login valid");

                        saveUserToLocalDb(apiUserResponse);


                    }else{
                        LoginViewModel.this.authState.setValue(AuthenticationState.INVALID_AUTHENTICATION);
                        Log.e(TAG, "login invalid");

                    }
                }
            }

            @Override
            public void onFailure(Call<ApiUserResponse> call, Throwable t) {
                LoginViewModel.this.authState.setValue(AuthenticationState.INVALID_AUTHENTICATION);
                Log.e(TAG, "network failure" + t.toString());
            }
        });
    }

    private void saveUserToLocalDb(ApiUserResponse apiUserResponse) {
        User user = apiUserResponse.getSingleUser();
        dataRepository.insertUser(user);
    }
}