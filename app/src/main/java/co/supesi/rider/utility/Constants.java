package co.supesi.rider.utility;

public  class Constants {

    private final static String PREF_JWT = "co.supesi.rider.PREFERENCE_JWT";

    public static String getPrefJwt() {
        return PREF_JWT;
    }
}
